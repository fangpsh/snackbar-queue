# @tozd/vue-snackbar-queue

This NPM package extends [Vuetify](http://vuetifyjs.com/) [Snackbar component](https://vuetifyjs.com/en/components/snackbars)
with a queue. Multiple snackbars can be queued and they are shown one after the other in the
order in which they were queued.

## Installation

This is a NPM package. You can install it using NPM:

```bash
$ npm install @tozd/vue-snackbar-queue
```

It requires `vue` and `vuetify` peer dependencies:

```bash
$ npm install vue vuetify
```

## Usage

First, you have to register the package as a Vue plugin:

```js
import Vue from 'vue';
import Vuetify from 'vuetify';
import VSnackbarQueue from '@tozd/vue-snackbar-queue';

Vue.use(Vuetify);
Vue.use(VSnackbarQueue);
```

Add the `v-snackbar-queue` component to your top-level app component:

```html
<template>
  <v-app>
    <router-view></router-view>
    <v-snackbar-queue></v-snackbar-queue>
  </v-app>
</template>
```

When you want to display a snackbar (or enqueue one, if another
snackbar is currently already being shown), call `$snackbar`.

## API

### `vm.$snackbar(message, color='info')`

Displays a snackbar with `message`. If another snackbar is currently
already being shown, the new snackbar is enqueued.

`color` is of the snackbar close button text. By default theme's `info`
color is used. Some other colors to consider are `success` and `error`.

## Props

The `v-snackbar-queue` component has the following props.

### `timeout` (type `Number`, default `6000`)

For how long to display each snackbar, in milliseconds.

### `pause` (type `Number`, default `300`)

How long should a pause between two consecutive snackbars be, in milliseconds.

## Slots

Using scoped slots you can control how snackbars are rendered.

### `default`

Slot to render the snackbar's message.

### `close`

Slot to render the snackbar's close button content.

## Examples

Using props and slots:

```html
<v-snackbar-queue :timeout="3000">
  <template slot="default" slot-scope="props">
    <v-icon small :color="props.snackbar && props.snackbar.color">info</v-icon> {{props.snackbar && props.snackbar.message}}
  </template>
  <template slot="close" slot-scope="props">
    <translate>close</translate>
  </template>
</v-snackbar-queue>
```
